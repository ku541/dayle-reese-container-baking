<?php

namespace Acme;

use Closure;
use Exception;
use ReflectionClass;

class Container
{
    /**
     * Collection of stored closures.
     *
     * @var array
     */
    protected $closures = [];

    /**
     * Collection of stored instances.
     *
     * @var array
     */
    protected $instances = [];

    /**
     * Bind an instance or a closure to the container.
     *
     * @param  string  $key
     * @param  object  $instance
     * @return void
     */
    public function bind(string $key, object $instance)
    {
        if ($instance instanceof Closure) {
            $this->closures[$key] = $instance;
        } else {
            $this->instances[$key] = $instance;
        }
    }

    /**
     * Check whether a binding exists in the container for the specified key
     *
     * @param  string  $key
     * @return bool
     */
    public function bound(string $key)
    {
        return isset($this->instances[$key]) || isset($this->closures[$key]);
    }

    /**
     * Resolve an instance from the container.
     *
     * @param  string  $key
     * @param  bool  $cache
     * @return object
     *
     * @throws Exception
     */
    public function resolve(string $key, bool $cache = false)
    {
        if (isset($this->instances[$key])) {
            return $this->instances[$key];
        }

        if (isset($this->closures[$key])) {
            return $cache ? $this->instances[$key] = $this->closures[$key]()
                : $this->closures[$key]();
        }

        if ($instance = $this->autoResolve($key)) {
            return $instance;
        }

        throw new Exception('Unable to resolve binding from container.');
    }

    /**
     * Recursively resolve dependancy chain.
     *
     * @param  string  $key
     * @return bool|object
     *
     * @throws Exception
     */
    public function autoResolve(string $key)
    {
        if (! class_exists($key)) {
            return false;
        }

        $reflectionClass = new ReflectionClass($key);

        if (! $reflectionClass->isInstantiable()) {
            return false;
        }

        if (! $constructor = $reflectionClass->getConstructor()) {
            return new $key;
        }

        $params = $constructor->getParameters();

        $args = [];

        try {
            foreach ($params as $param) {
                $paramClass = $param->getClass()->getName();

                $args[] = $this->resolve($paramClass);
            }
        } catch (Exception $e) {
            throw new Exception('Unable to resolve complex dependencies.');
        }

        return $reflectionClass->newInstanceArgs($args);
    }
}