<?php

namespace Acme\Tests;

use stdClass;
use Acme\Baz;
use Acme\Foo;
use Acme\Qux;
use Acme\Container;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    /** @test */
    public function a_container_can_be_created()
    {
        $container = new Container;

        $this->assertInstanceOf(Container::class, $container);
    }

    /** @test */
    public function an_instance_can_be_bound_to_the_container()
    {
        $instance = new stdClass;
        $container = new Container;

        $container->bind(stdClass::class, $instance);

        $this->assertTrue($container->bound(stdClass::class));
    }

    /** @test */
    public function an_instance_can_be_resolved_from_the_container()
    {
        $instance = new stdClass;
        $container = new Container;

        $container->bind(stdClass::class, $instance);

        $this->assertSame($instance, $container->resolve(stdClass::class));
    }

    /** @test */
    public function an_exception_is_thrown_when_an_instance_cannot_be_resolved_from_the_container()
    {
        $instance = new stdClass;
        $container = new Container;

        $container->bind(stdClass::class, $instance);

        $this->expectExceptionMessage('Unable to resolve binding from container.');

        $container->resolve('Class');
    }

    /** @test */
    public function a_closure_can_be_resolved_from_the_container()
    {
        $container = new Container;
        $closure = function () { return new stdClass; };

        $container->bind(stdClass::class, $closure);

        $this->assertInstanceOf(
            stdClass::class,
            $container->resolve(stdClass::class)
        );
    }

    /** @test */
    public function a_singleton_can_be_resolved_from_the_container()
    {
        $container = new Container;
        $closure = function () { return new stdClass; };

        $container->bind(stdClass::class, $closure);

        $this->assertSame(
            $container->resolve(stdClass::class, true),
            $container->resolve(stdClass::class)
        );
    }

    /** @test */
    public function a_class_instance_can_be_resolved_by_name_without_a_binding_from_the_container()
    {
        $container = new Container;

        $this->assertInstanceOf(
            Foo::class,
            $container->resolve(Foo::class)
        );
    }

    /** @test */
    public function dependancies_of_dependancies_can_be_resolved_from_the_container()
    {
        $container = new Container;

        $this->assertInstanceOf(
            Baz::class,
            $container->resolve(Baz::class)
        );
    }

    /** @test */
    public function an_exception_is_thrown_when_dependancies_of_dependancies_cannot_be_resolved_from_the_container()
    {
        $container = new Container;

        $this->expectExceptionMessage('Unable to resolve complex dependencies.');

        $container->resolve(Qux::class);
    }
}